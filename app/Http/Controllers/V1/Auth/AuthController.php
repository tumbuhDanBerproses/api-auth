<?php

namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'registerByEmail', 'registerByPhone']]);
    }

    public function register(Request $request)
    {
        if ($request->email && $request->phone) {
            try {
                $validated = $this->validate($request, [
                    'name' => 'required|max:255',
                    'email' => 'required|max:255|email|unique:users',
                    'phone' => 'required|max:255|phone|unique:users',
                    'password' => "required|min:6|same:password_confirm",
                    'password_confirm' => "required|min:6|same:password",
                ]);
            
                $user = new User();
                $user->name = $validated['name'];
                $user->email = $validated['email'];
                $user->phone = $validated['phone'];
                $user->password = Hash::make($validated['password']);
    
                if ($user->save()) {
                    return response()->json([
                        'status' => true,
                        'message' => 'User created successfully',
                        'data' => $user
                    ]);
                } else {
                    return response()->json([
                        'status' => true,
                        'message' => 'User created failed',
                        'data' => NULL
                    ]);
                }
            } catch (\Exception $e) {
                return response()->json([
                    'status' => false,
                    'message' => $e->getMessage(),
                    'data' => $e->errors()
                ]);
            }
        } elseif ($request->email && !$request->phone) {
            $this->registerByEmail($request);
        } elseif ($request->phone && !$request->email) {
            $this->registerByPhone($request);
        } else {
            try {
                $validated = $this->validate($request, [
                    'name' => 'required|max:255',
                    'email' => 'required|max:255|email|unique:users',
                    'phone' => 'required|max:255|phone|unique:users',
                    'password' => "required|min:6|same:password_confirm",
                    'password_confirm' => "required|min:6|same:password",
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => false,
                    'message' => $e->getMessage(),
                    'data' => $e->errors()
                ]);
            }
        }
    }

    public function registerByEmail(Request $request)
    {
        try {
            $validated = $this->validate($request, [
                'name' => 'required|max:255',
                'email' => 'required|email|unique:users|max:255',
                'password' => "required|min:6|same:password_confirm",
                'password_confirm' => "required|min:6|same:password",
            ]);
        
            $user = new User;
            $user->name = $validated['name'];
            $user->email = $validated['email'];
            $user->password = Hash::make($validated['password']);

            if ($user->save()) {
                return response()->json([
                    'status' => true,
                    'message' => 'User created successfully',
                    'data' => $user
                ]);
            } else {
                return response()->json([
                    'status' => true,
                    'message' => 'User created failed',
                    'data' => NULL
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => $e->errors()
            ]);
        }
    }

    public function registerByPhone(Request $request)
    {
        try {
            $validated = $this->validate($request, [
                'name' => 'required|max:255',
                'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:13|unique:users',
                'password' => "required|min:6|same:password_confirm",
                'password_confirm' => "required|min:6|same:password",
            ]);

            $user = new User;
            $user->name = $validated['name'];
            $user->phone = $validated['phone'];
            $user->password = Hash::make($validated['password']);

            if ($user->save()) {
                return response()->json([
                    'status' => true,
                    'message' => 'User created successfully',
                    'data' => $user
                ]);
            } else {
                return response()->json([
                    'status' => true,
                    'message' => 'User created failed',
                    'data' => NULL
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => $e->errors()
            ]);
        }
    }

    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'nullable|email|string',
                'phone' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                'password' => 'required|string',
            ]);

            if ($request->email && !$token = Auth::attempt($request->only(['email', 'password']))) {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid email or password',
                    'data' => NULL
                ], 401);
            } elseif ($request->phone && !$token = Auth::attempt($request->only(['phone', 'password']))) {
                return response()->json([
                    'status' => false,
                    'message' => 'Invalid phone or password',
                    'data' => NULL
                ], 401);
            }

            return $this->respondWithToken($token);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => $e->errors()
            ]);
        }
    }

    public function logout()
    {
        auth()->logout();
        auth()->logout(true);

        return response()->json([
            'status' => true,
            'message' => 'User logout in successfully',
            'data' => NULL
        ]);
    }

    public function refreshToken()
    {
        return $this->respondWithToken(auth()->refresh());
    }
}