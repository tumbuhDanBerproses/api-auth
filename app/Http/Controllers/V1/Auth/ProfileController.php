<?php

namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index()
    {
        try {
            $user = auth()->userOrFail();
            return response()->json([
                'status' => true,
                'message' => NULL,
                'data' => $user
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => NULL
            ]);
        }
    }

    public function update(Request $request)
    {
        try {
            $user = auth()->userOrFail();
            $validated = $this->validate($request, [
                'name' => 'nullable|max:255',
                'email' => 'nullable|email|max:255|unique:users,email,' . $user->id . ',id',
                'phone' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:13|unique:users,phone,' . $user->id . ',id',
            ]);

            $user->name = $validated['name'] ? $validated['name'] : $user->name;
            $user->email = $validated['email'] ? $validated['email'] : $user->email;
            $user->phone = $validated['phone'] ? $validated['phone'] : $user->phone;
            if ($user->save()) {
                return response()->json([
                    'status' => true,
                    'message' => 'Profile updated successfully',
                    'data' => $user
                ]);
            } else {
                return response()->json([
                    'status' => true,
                    'message' => 'Profile updated failed',
                    'data' => NULL
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
                'data' => NULL
            ]);
        }
    }
}