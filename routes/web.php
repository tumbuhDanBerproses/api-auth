<?php

/** @var \Laravel\Lumen\Routing\Router $router */
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group([
    'as' => 'v1',
    'prefix' => 'v1',
    'namespace' => 'V1',
],  function () use ($router) {
    $router->group([
        'as' => 'auth',
        'prefix' => 'auth',
        'namespace' => 'Auth',
    ],  function () use ($router) {
        $router->group([
            'as' => 'register',
            'prefix' => 'register',
        ],  function () use ($router) {
            $router->post('email', ['uses' => 'AuthController@registerByEmail', 'as' => 'email']);
            $router->post('phone', ['uses' => 'AuthController@registerByPhone', 'as' => 'phone']);
            $router->post('', ['uses' => 'AuthController@register']);
        });

        $router->post('login', ['uses' => 'AuthController@login', 'as' => 'login']);
        $router->get('logout', ['uses' => 'AuthController@logout', 'as' => 'logout']);

        $router->post('refresh-token', ['uses' => 'AuthController@refreshToken', 'as' => 'refresh-token']);

        $router->group([
            'as' => 'password',
            'prefix' => 'password',
        ],  function () use ($router) {
            $router->post('email', 'PasswordController@postEmail');
            $router->put('reset', ['uses' => 'PasswordController@postReset', 'as' => 'reset']);
        });
    });
});

$router->get('reset-password/{token}', [function (Request $request, $token) {
    return response()->json([
        'status' => true,
        'message' => 'Reset your password with this method',
        'data' => [
            'method' => 'PUT',
            'url' => route('v1.auth.password.reset'),
            'data_sent' => [
                'token' => $token,
                'email' => $request->email,
                'password' => 'Your new password',
                'password_confirmation' => 'Confirm your new password'
            ]
        ]
    ]);
}, 'as' => 'password.reset']);

$router->group([
    'middleware' => 'auth:api',
],  function () use ($router) {
    $router->group([
        'as' => 'v1',
        'prefix' => 'v1',
        'namespace' => 'V1',
    ],  function () use ($router) {
        $router->group([
            'as' => 'auth',
            'prefix' => 'auth',
            'namespace' => 'Auth',
        ],  function () use ($router) {
            $router->group([
                'as' => 'profile',
                'prefix' => 'profile',
            ],  function () use ($router) {
                $router->get('', ['uses' => 'ProfileController@index', 'as' => 'profile.index']);
                $router->put('', ['uses' => 'ProfileController@update', 'as' => 'profile.update']);
            });
        });
    });
});