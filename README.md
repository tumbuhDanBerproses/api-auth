# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Table of Contents
* [Introduction](#introduction)
* [Infrastructure](#infrastructure)
* [Installation](#installation)
* [How to Run](#how-to-run)
* [Live Demo](#live-demo)
* [Postman Collection](#postman-collection)

### Introduction
This project explains about authentication. There are some features, such as: Create User by Email and Phone, Login, Logout, Update Profile, Reset Password.

### Infrastructure
I try making this project as a challenge for me. I use [`PHP`](https://www.php.net/) as programming language and [`Lumen`](https://lumen.laravel.com) as framework. Database in this project uses [`MySQL`](https://www.postgresql.org).

### Installation
1. Install `PHP`, `Lumen`, and `MySQL`.
    * [`PHP`](https://www.php.net/downloads)
    * [`Lumen`](https://lumen.laravel.com/docs/8.x#installing-lumen)
    * [`MySQL`](https://dev.mysql.com/doc/)
1. Cloning this project out with the following command:
    * Using SSH URL
    ```bash
    $ git clone git@gitlab.com:tumbuhDanBerproses/api-auth.git
    ```
    * Using HTTPS URL
    ```bash
    $ git clone https://gitlab.com/tumbuhDanBerproses/api-auth.git
    ```
1. Do the command to install the dependencies:
    ```bash
    $ composer install
    ```
1. Set up the database configuration at `.env`. For example:

        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=api_auth
        DB_USERNAME=root
        DB_PASSWORD=

1. Set up the SMTP configuration at `.env`. For example:

        MAIL_MAILER=smtp
        MAIL_HOST=smtp.gmail.com
        MAIL_PORT=587
        MAIL_USERNAME=email@gmail.com
        MAIL_PASSWORD=password
        MAIL_ENCRYPTION=tls
        MAIL_FROM_ADDRESS=email@gmail.com
        MAIL_FROM_NAME="${APP_NAME}"

1. Do the command to create and migrate database:
    ```bash
    $ php artisan migrate
    ```


### How to Run
Run server:
```bash
$ php -S localhost:1337 -t public
```

### Live Demo
You can test my project [here](http://tumbuhdanberproses.azisalvriyanto.net/api-auth/public).

### Postman Collection
You can get postman collection [here](https://www.getpostman.com/collections/10e46b3c95b7242d5c9c).
